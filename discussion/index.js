// JSON Objects

// Converting JS Data into stringified JSON
// Stringified JSON is a JavaScript Object converted into a string to be used
// by the receiving back-end application or other functions of a javascript application.
batchesArr = [{batchname: 'Batch 145'},
			  {batchname: 'Batch 146'},
			  {batchname: 'Batch 147'}];
let stringifiedData = JSON.stringify(batchesArr);

// Convert stringifiedData to JS Objects
let fixedData = JSON.parse(stringifiedData);